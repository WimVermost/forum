var Question = require('../models/question');

function create(req, res) {

	// save a new instance of this model
	var newQuestion = new Question({
		user: req.body.user,
		title: req.body.tb_question,
		date: {
			type: Date,
			default: Date.now
		},
	});

	newQuestion.save(function (err, question) {
		if (err) return console.error(err);
		res.send(question);
	});
}
module.exports.create = create;

function getAll(req, res) {
	Question.find(function (err, questions) {
		if (err) return console.error(err);
		res.send(questions);
	});
}
module.exports.getAll = getAll;