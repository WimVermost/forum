var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

gulp.task('minify', function() {
 gulp.src('public/stylesheets/style.css')
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('public/stylesheets/'));
});

gulp.task('sass', function () {
  return gulp.src('public/stylesheets/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/stylesheets/'));
	
});

gulp.task('default',function() {
    gulp.watch('public/stylesheets/*.scss',['sass']);
    gulp.watch('public/stylesheets/*.css',['minify']);
});

gulp.task('compress', function() {
  return gulp.src('public/javascripts/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('public/javascripts'));
});