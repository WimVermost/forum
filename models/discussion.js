var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// we define a mongoose schema
var discussionSchema = mongoose.Schema({
	user		: { type: String },
	title		: { type: String },
	date		: { type : Date, default: Date.now },
	latitude	: { type: Number },
	longitude	: { type: Number }
});
// then we compile this schema into a model
var Discussion = mongoose.model('Discussion', discussionSchema);

module.exports = Discussion;

