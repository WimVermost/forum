'use strict';

module.exports = function (http) {
	var io = require('socket.io')(http);
	var onlineUsers = 0;

	//when someone connects...
	io.on('connection', function (socket) {
		var discussion = require('./models/discussion');
		var question = require('./models/question');
		onlineUsers++;
		
		discussion.find()
			.sort({date: -1}).exec(function(err, discussions) {
			console.log("Questions are loading...");
			socket.emit('load_discussions', discussions);
		});
		
		
		io.emit('usercount', onlineUsers);
		//usercount laten updaten bij nieuwe client.

		
		socket.on('new_discussion', function (user, title, x, y) {

			discussion.create({
				user: user,
				title: title,
				latitude: x,
				longitude: y}, function(err, discussion) {
							 			 
				io.emit("discussionCreated", discussion);			 
			});		
			
		});
					
		socket.on('new_question', function (user, questionAsked, discussion_id) {
			
			question.create({
				user: user,
				question: questionAsked,
				status:"open",
				discussion_id: discussion_id}, function(err, question){

				io.emit("questionCreated", question);		
				
			});
				
		});

		socket.on('new_comment', function (user, commentPlaced, question_id) {
			
			var d = new Date();
			var n = d.toISOString();
			
			var newComment = { question_id: question_id, user:user, message:commentPlaced, date: n};
	
			question.findByIdAndUpdate(
				question_id,
				{$push: {comments: newComment}},
			function(err, question){
				 io.emit("commentCreated",newComment);
			 }
			);

		});

		socket.on('change_status', function (user, question_id, status) {
			console.log(question_id);
			question.findByIdAndUpdate(
				question_id,
				{status: status},
				function(err, question){

					io.emit("statusChanged",question_id,status);

				}
			);
		});
		
		//when someone leaves :(
		socket.on('disconnect', function () {
			onlineUsers--;
			io.emit('usercount', onlineUsers);
		});

	

	});

	return io;
}