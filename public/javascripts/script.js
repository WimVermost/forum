$(document).ready(function () {

	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			var curLat = position.coords.latitude;
			var curLong = position.coords.longitude;
			
				var key = "AIzaSyA-4ZnK74Tau3lW8qARsUjotgISPJ34x20";
    			var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+curLat+","+curLong+"&key="+key;
		
				window.jQuery.ajax({
					url: url,
					dataType: "json",
					success: function (data) {
					$("#disc_loc").text("Discussies in de buurt van "+data.results[1].address_components[0].long_name);
					
				}});
			
			
		});
	}
	
	$(".panel-body").html('');
	// Socket connection
	var socket = io.connect('');
	jQuery("time.timeago").timeago();

	
	socket.on('usercount', function(users){
		$(".badge").text(users);
		
	});

	socket.on('load_discussions', function(discussions) {
		
		if (navigator.geolocation)
		{

			navigator.geolocation.getCurrentPosition(function(position){

				curLat = position.coords.latitude;
				curLong = position.coords.longitude;

				$.each(discussions, function(i, discussion){
					
					var curPos = new google.maps.LatLng(curLat, curLong);
					var discPos = new google.maps.LatLng(discussion.latitude,discussion.longitude);
					var dist = Math.round(google.maps.geometry.spherical.computeDistanceBetween(curPos, discPos));

					if(dist < 1000){

						var listitem =  '<div class="row">'+
							'<div class="card flex-column">'+
							'<h3>' + discussion.title+ '</h3>'+
							'<span class="discussion_user">Door: '+ discussion.user+'</span>'+
							'<time class="timeago" datetime="'+ discussion.date.toLocaleString() +'"></time>'+
							'<a href="/discussion/'+discussion._id+'" class="open_button">Open discussie</a>'+
							'</div>'+
							'</div>';
						$('#list_discussion').append(listitem);
					};

				});
			});
		}
		else
		{
			$('#list_discussion').html("Geolocation must be on.");
		}
	});




	
	socket.on('discussionCreated', function (newDiscussion) {
		 
		var question = "<div class='row'><div class='card flex-column'><h3>"+newDiscussion.title+"</h3><span class='discussion_user'>Door: "+newDiscussion.user+"</span><time class='timeago' datetime='"+newDiscussion.date.toLocaleString()+"'>Nieuw!</time><a href='/discussion/"+newDiscussion._id+"'  class='open_button'>Open discussie</a></div></div>";
		
		$("#list_discussion").prepend(question);
		
	});
	
	
	socket.on('questionCreated', function (newQuestion) {

		var question = "<div class='row'><div class='card_question flex_column'><span id='ribbon_"+newQuestion._id+"'  class='ribbonOpen' data-id='"+ newQuestion._id +"'><span>open</span></span><h3>"+newQuestion.question+"</h3><span class='question_user'>Door: "+newQuestion.user+"</span><br><time class='timeago' datetime='"+newQuestion.date.toLocaleString()+"'>minder dan een minuut geleden</time><div class='comments' id='comments_"+ newQuestion._id+"'></div><div class='input-group'><input type='text' id='tb_answer_"+ newQuestion._id+"' class='form-control tb-comment' placeholder='Antwoorden'><span class='input-group-btn'><button class='btn btn-primary btn-comment' type='button' id='btn_answer' data-id='"+ newQuestion._id+"'>Antwoord</button></span></div>";

		$("#list_question").prepend(question);

	});

	socket.on('commentCreated', function (newComment) {

		var comment = "<div class='row'><div class='flex-comment'><span class='discussion_user'>"+newComment.user+":</span><p>"+newComment.message+"</p><time class='timeago' datetime='"+newComment.date.toLocaleString()+"'>minder dan een minuut geleden</time></div></div>";

		$("#comments_"+newComment.question_id).append(comment);

	});

	socket.on('statusChanged', function (question_id, status) {

		$("#ribbon_"+question_id).html("<span>"+status+"</span>");
		if(status == "closed"){
			$("#ribbon_"+question_id).attr('class', 'ribbonClosed');
		}else{
			$("#ribbon_"+question_id).attr('class', 'ribbonOpen');
		}

	});

	$("#list_question").on("click",".ribbonOpen", function(){

		socket.emit('change_status', activeUser, $(this).data("id"), "closed");

	});
	$("#list_question").on("click",".ribbonClosed", function(){

		socket.emit('change_status', activeUser, $(this).data("id"), "open");

	});

	$("#list_question").on("click","#btn_answer", function(){

		var id = $(this).data("id");
		
		if($('#tb_answer_'+id).val() != ''){
			socket.emit('new_comment', activeUser, $('#tb_answer_'+id).val(), $(this).data("id"));
			$('#tb_answer_'+id).val('');
		}
		
	});
	
	
	$("#btn_discussion").on("click", function () {
		
		if ($('#tb_discussion').val() != '') {

				if(curLat != 0) {
					
					socket.emit('new_discussion', activeUser, $('#tb_discussion').val(), curLat, curLong);
					$('#tb_discussion').val('');
				}
		}
		
	});
	
	$("#btn_question").on("click", function () {
		
		if ($('#tb_question').val() != '') {
			socket.emit('new_question', activeUser, $('#tb_question').val(), $(this).data("id") );
			$('#tb_question').val('');
		}
		
	});
	
});