module.exports = function(app, passport) {

    // HOME PAGE ============================
    app.get('/', function(req, res) {
        res.render('index.ejs'), {
			title: "Welcome!",
		}
    });

    // LOGIN ===============================
    app.get('/login', function(req, res) {
        res.render('login.ejs', { 
			message: req.flash('loginMessage'),
			title: "Login to use the Q&A liveapp",
		}); 
    });

    // process the login form
     app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/discussion', // redirect to the secure discussion section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
	
	// process the login form
	app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/discussion', // redirect to the secure discussion section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    // SIGNUP ==============================
    app.get('/signup', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { 
			message: req.flash('signupMessage'),
			title: "Signup to use the Q&A liveapp",
		});
    });


    // Discussion SECTION =====================
    // we will use route middleware to verify if user is loggen in (the isLoggedIn function)

    app.get('/discussion', isLoggedIn, function(req, res) {
		
        var discussion = require('../models/discussion');
		var gravatar = require('gravatar');
		var secureUrl = gravatar.url(req.user.local.email, {s: '55', r: 'x', d: 'retro'}, true);
		// alle discussie's zoeken en die sorteren
		discussion.find().sort({date: -1}).exec(function (err, discussion) {
				res.render('discussion.ejs', {
				user : req.user, // get the user out of session and pass to template	
				userpic: secureUrl,
				discussion: discussion,	
				title: "List of discussions",
				discussion_id: req.params.id	
    		});
		});
	});
	
	app.get('/discussion/:id', isLoggedIn, function(req, res) {

		var question = require('../models/question');
        var discussion = require('../models/discussion');
		var gravatar = require('gravatar');
		var secureUrl = gravatar.url(req.user.local.email, {s: '55', r: 'x', d: 'retro'}, true);
		//met req.params.id kunnen we het gedeelte achter /discussion/ uit de link halen
	    //dit is de discussion id
		question.find({ 'discussion_id': req.params.id }, function(err, question){
			//alle vragen in mongo ophalen die een bepaalde discussion id hebben.
			discussion.findById(req.params.id, function (err, discussion) {
				// titel van discussie opvragen
				res.render('question.ejs', {
            	user : req.user, // get the user out of session and pass to template	 
				question: question, // array met vragen doorsturen naar template
				title: "Questions",
				userpic: secureUrl,
				currentDiscussion: discussion.title,
				discussion_id: req.params.id
					
				});
			
			});
					
		}).sort({date: -1});	
        
	});
	
    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    // FACEBOOK ROUTES =====================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/discussion',
            failureRedirect : '/'
        }));

    // route for logging out
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
	
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}