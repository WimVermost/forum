var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// we define a mongoose schema
var questionSchema = mongoose.Schema({
	user			: { type: String},
	question		: { type: String},
	discussion_id	: { type: String},
	date			: { type : Date, default: Date.now},
	comments 		: [{ }],
	status			: { type: String},
	gravatar		: { type: String},
});
// then we compile this schema into a model
var Question = mongoose.model('Question', questionSchema);

module.exports = Question;

