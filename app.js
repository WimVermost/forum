var express = require('express');
var app = express();

var http = require('http').createServer(app);
var io = require('socket.io')(http);
var path = require('path');

var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var configDB = require('./config/database.js');
var favicon = require('serve-favicon');
var gravatar = require('gravatar');

//connectie met Mongodb 
mongoose.connect('mongodb://localhost/Q&A');

require('./config/passport')(passport); // pass passport for configuration

// view engine setup
app.set('views', path.join(__dirname, 'views'));

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// required for passport
app.use(session({ secret: 'wimwimw1mwimwimwim' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// gebruik public map
app.use(express.static(__dirname + '/public'));

// routes 
require('./routes/routes.js')(app, passport); 

// alle socket events staan in socket.js
var io = require('./socket')(http);

// launch the server
// http omdat we de poort hergebruiken, zoals voor socket, heb ik hier 2 dagen naar gezocht lol
http.listen(port, function() {
   	console.log("The server runs on: " + port)
});

module.exports = app;
